package trabalhoaed;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author ghabr
 */

public class TrabalhoAED {
    
    
    //Função criada para retornar o horário atual do computador
    public String horario(){
        Calendar c = Calendar.getInstance();
        String horario = ("Horário: "+ (Integer.toString(c.get(Calendar.HOUR_OF_DAY))) + ":" 
                    + (Integer.toString(c.get(Calendar.MINUTE)))+ ":" +((Integer.toString(c.get(Calendar.SECOND)))));
        return horario;
    }
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        //Pegando a função de horario e colocando em uma variável para ser usado vária vezes em outro local
        TrabalhoAED horario = new TrabalhoAED();
        
        //numArq = Número do arquivo para verificar se já existe
        int numArq = 1;
        
        //Pegando o arquivo no local indicado
        File arqSai = new File("src/trabalhoaed/log"+numArq+".txt");
        
        //Nesse momento, eu verifico se o arquivo já existe
        boolean existe = arqSai.exists();
        FileWriter fw = new FileWriter(arqSai, true);
        BufferedWriter bw = new BufferedWriter(fw);
        
        //Utilizo o laço de repetição While, para verificar se o arquivo de número passado entre 
        while(existe ==true){
            numArq++;
            arqSai = new File("src/trabalhoaed/log"+numArq+".txt");
            if(arqSai.exists() ==false){
                existe = false;
                fw = new FileWriter(arqSai, true);
                bw = new BufferedWriter(fw);
            }
        }
        //Variável criada para deixar o While infinito até que o usuário queira sair
        String sair = "nao";
        
        Scanner entrada = new Scanner(System.in);
        
        //Instanciamento da classe Pilha na classe principal
        PilhaIngressos Pilha = new PilhaIngressos();
        
        //Instanciamento da classe Fila na classe principal
        FilaPessoas Fila = new FilaPessoas();
        
        //Instanciamento da classe Lista na classe principal
        EstruturaLista Lista = new EstruturaLista(65);
        
        //Criando a fila com 80 posições
        Fila.queue(80);
        
        //Método para criar pilha
        Pilha.criarPilha();
        
        //Neswse momento, a Pilha de ingressos será preenchida com números aleatórios
        Pilha.push();
        
        //Gravar no log a informação
        bw.write("================================================================================\n" +
            "                            Inicio da execução\n" +
            "                             "+horario.horario()+
            "\n ================================================================================\n");
        bw.newLine();
        bw.newLine();
        
        //Estrutura de repetição para não sair do programa enquanto o usuário não quiser
        while(sair.equals("nao")){
            //Gravar no log a informação
            bw.write("================================================================================\n" +
                     "              Nesse momento a lista está com os seguintes dados:\n" +
                   "\n"+Lista.mostrarDados()+        
                     "                           "+horario.horario()+
                   "\n ================================================================================\n");
            bw.newLine();
            bw.newLine();
            
            //Mostrando as opções que o usuário pode escolher
            System.out.println("Escolha uma opção: "
                + "\n 1 - Entrar no cadastramento "
                + "\n 2 - Mostrar pessoas na Fila "
                + "\n 3 - Entregar ingresso  "
                + "\n 4 - Mostrar Lista dos Agraciados "
                + "\n 5 - Mostrar Pilha de ingressos "
                + "\n 6 - Sair da aplicação");
            
            //Receber a opção passada pelo usuário
            int opcao = entrada.nextInt();
            
            
            //Estrutura condicional criada para realizar a tarefa escolhida pelo usuário
            switch(opcao){
                case 1:
                    //Gravar no log a informação
                    bw.write("================================================================================\n" +
                             "                Usuário escolhe a opção de cadastrar pessoas"+
                           "\n                           "+horario.horario()+
                           "\n===============================================================================\n");
                    bw.newLine();
                    bw.newLine();
                    System.out.println("Deseja cadastrar quantas pessoas?");
                    
                    //Receber a quantidade de pessoas que ele deseja cadastrar
                    int qtdPessoas = entrada.nextInt();
                    
                    //Estrutura de repetição criada para ele cadastrar o número de pessoas passado acima
                    while(qtdPessoas !=0){
                        System.out.println("Olá, para a pessoa entrar na fila, é necessário que ela passe os dados dela: \n");
                        System.out.print("Digite o nome da pessoa que irá entrar na fila:");
                        String nome = entrada.next();
                        System.out.print("Agora, digite o email dela:");
                        String email = entrada.next();
                        System.out.println("Por último, se ela possui um número de telefone, informe por gentileza, caso não, coloque o número 0;");
                        long numero = entrada.nextLong();
                        
                        //Colocar no nó da Fila os dados passado pelo o usuário
                        Fila.enqueue(nome, email, numero);
                        qtdPessoas --;
                        
                    }        
                    break;
                    
                case 2:
                    
                    //Sistema só deixará mostrar as pessoas na Fila, caso a Fila possua, uma ou mais pessoas
                    if(Fila.isEmpty()==true){
                        System.out.println("Cadastre novas pessoas para ela entrar na fila \n \n");
                        
                        //Gravar no log a informação
                        bw.write("================================================================================\n" +
                                 "                 Usuário escolhe a opção de mostrar as pessoas \n"
                                +"                    na fila, mas não possui ninguém nela"+
                               "\n================================================================================\n");
                    }
                    else{
                        //Gravar no log a informação
                        bw.write("================================================================================\n" +
                                 "              Usuário escolhe a opção de mostrar as pessoas já"
                                +"\n               cadastradas que estão na fila para pegar o      "
                                +"\n                                 ingresso"+
                                 "\n               "+horario.horario()+
                               "\n================================================================================\n");
                        bw.newLine();
                        bw.newLine();
                        
                        //Chamar o método da classe Fila para mostrar todas as pessoas presente
                        System.out.println(Fila.print());
                    }
                    break;
                          
                
                case 3:
                    
                    //Sistema só deixará entregar ingressos para pessoas cadastradas na Fila.
                    if(Fila.isEmpty()==true){
                        System.out.println("Fila vazia, cadastre primeiro antes de entregar o ingresso \n\n");
                        //Gravar no log a informação
                        bw.write("================================================================================\n" +
                                 "            Usuário tentou entregar um ingresso com ninguém na fila"+
                               "\n================================================================================\n");
                        bw.newLine();
                        bw.newLine();
                    }
                    else{
                        //Gravar no log a informação
                        bw.write("================================================================================\n" +
                            "               BilúBilú entrega o ingresso da sala "+ Pilha.getNumSalaTop()+ " para o(a) "+ Fila.getNomePrimeiro()+
                            "\n                                "+horario.horario()+
                           "\n================================================================================\n");
                        bw.newLine();
                        bw.newLine();
                        System.out.println("Ingresso da sala "+Pilha.getNumSalaTop()+" para o(a) "+Fila.getNomePrimeiro());
                        
                        //Inserir todos os dados na Lista
                        Lista.inserirDados(0, Fila.getNomePrimeiro(), Fila.getEmailPrimeiro(), Fila.NumeroPrimeiro(), Pilha.getNumSalaTop());
                        
                        //Remover o ingresso do topo
                        Pilha.pop();
                        
                        //Remover a primeira pessoa da Fila
                        Fila.dequeue();
                    }
                    break;
                case 4:
                    //Gravar no log a informação
                    bw.write("================================================================================\n" +
                             "              Usuário pede para mostrar a lista dos agraciados"+
                             "\n                          "+horario.horario()+
                           "\n================================================================================\n");
                    bw.newLine();
                    bw.newLine();
                    
                    //Mostrar todos os dados da Lista
                    System.out.println(Lista.mostrarDados());
                    break;
                case 5:
                    //Gravar no log a informação
                    bw.write("================================================================================\n" +
                             "                Usuário pede para mostrar a Pilha de ingressos"+
                             "\n                            "+horario.horario()+
                           "\n================================================================================\n");
                    bw.newLine();
                    bw.newLine();
                    System.out.println("\n");
                    Pilha.print();
                    break;
                case 6:
                    //Gravar no log a informação
                    bw.write("================================================================================\n" +
                             "                          Usuário encerra o sistema"+
                             "\n                             "+horario.horario()+
                           "\n================================================================================\n");
                    sair = "sim";
                    
                    //Fechar o arquivo de log
                    bw.close();
                    fw.close();
                    break;
                 
            }   
        }
    }
}
