package trabalhoaed;


/**
 *
 * @author ghabr
 */
class Ingressos {
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private int numeroSala;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // num = numero da sala
    public Ingressos(int num) {
        this.numeroSala = num;
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para resgatar o número da sala
    public int getNumeroSala() {
        return this.numeroSala;
    }

    // Método para registrar o número da sala
    // numberSala  = número da sala
    public void setNumeroSala(int numberSala) {
        this.numeroSala = numberSala;
    }
    

}