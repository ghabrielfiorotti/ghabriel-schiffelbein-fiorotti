package trabalhoaed;

/**
 *
 * @author ghabr
 */
public class FilaPessoas {
    private NoFila Fila[];
    
    private
        int inicioDaFila;
        int finalDaFila;
        int quantidadeDeNos;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
        
    public FilaPessoas() { }
        
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    
    // Método para acessar a fila.
    public NoFila[] getFila() {
        return this.Fila;
    }
    
    // Método para inicializar (registrar) a fila.
    public void setFila(NoFila[] _fila) {
        this.Fila = _fila;
    }

    // Método para resgatar o início da fila.
    public int getInicioDaFila() {
        return this.inicioDaFila;
    }

    // Método para alterar o início da fila.
    public void setInicioDaFila(int _inicioDaFila) {
        this.inicioDaFila = _inicioDaFila;
    }

    // Método para resgatar o final da fila
    public int getFinalDaFila() {
        return this.finalDaFila;
    }

    // Método para alterar o final da fila.
    public void setFinalDaFila(int _finalDaFila) {
        this.finalDaFila = _finalDaFila;
    }

    // Método para resgatar a quantidade de nós.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }

    // Método para alterar a quantidade de nós.s
    public void setQuantidadeDeNos(int _quantidadeDeNos) {
        this.quantidadeDeNos = _quantidadeDeNos;
    }

    
   /*-----------------------------------
           OPERAÇÕES DO TAD FILA C
    -----------------------------------*/
    
    // Método para criar a fila
    // _qDN = quantidade de nós
    public void queue(int _qDN){
        
        this.setFila(new NoFila[_qDN]);
        
        this.setInicioDaFila(0);
        this.setFinalDaFila(-1);
        this.setQuantidadeDeNos(0); 
        
    }
    
    // Método para verificar se a fila está vazia.
    public boolean isEmpty(){
        
        // Quando o final da fila passa a ser menor "em valor"
        // do que o início da fila, então a fila está vazia.
        return ( this.getFinalDaFila() < this.getInicioDaFila() );
        
    }
    
    // Método para verificar se a fila está cheia.
    public boolean isFull(){
        
        // Caso existe algum elemento na última posição do vetor,
        // dizemos que a Fila está cheia.
        return ( this.getFinalDaFila() == (this.getFila().length-1) );
        
    }

       
    // Método para enfileirar dados.
    public boolean enqueue(String _n, String _mail, long num){
        
        // Se a fila não estiver cheia,
        if (!this.isFull()){
            
            // Diz quala posição válida para inserir um nó.
            this.setFinalDaFila(this.getFinalDaFila()+1);
            
            // Cria um novo nó, insere os dados do usuário e
            // insere o nó na posição 'finalDaFila'.
            this.getFila()[this.getFinalDaFila()] = new NoFila(_n, _mail, num);

            // Atualiza a quantidade de nós da fila.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()+1);
            
            return true;
        }
        
        return false;
    }
    
    // Método para desenfileirar dados.
    // O nó a ser removido sempre será o que está na 1ª posição da Fila.
    public boolean dequeue(){
        
        // Se foi possível apresentar os dados do nó
        // que está na 1ª posição da Fila, efetiva a remoção.
        if (this.front()){
        
            // Percorre a fila, do início para o final,
            // deslocando os elementos da direita para esquerda,
            // para evitar buraco na Fila.
            for (int i = this.getInicioDaFila(); i < this.getFinalDaFila(); i++)
                this.getFila()[i] = this.getFila()[i+1];
            
            // Atualiza o valor do final da fila.
            this.setFinalDaFila(this.getFinalDaFila()-1);

            // Atualiza a quantidade de nós da fila.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()-1);
            
            return true;
        }
        
        return false;
    }

    
    // Método que apresenta os dados do objeto que está na 1ª posição da Fila.
    public boolean front(){
        
        // Se a fila não estiver vazia, apresenta os dados.
        if (!this.isEmpty()) {
            return true;
        }
        
        return false;
    }
    
    // Método que retorna todos os dados da Fila em formato de "String".
    public String print(){
        
        String informacoes = "";

        // Se a fila não estiver vazia, percorre a fila.
       
        // Percorrendo a fila para montar a String.
        for (int i = this.getInicioDaFila(); i <= this.getFinalDaFila(); i++)
            
            informacoes += "Posição: "+ i+ "\n"+this.getFila()[i].getObjeto().toStringDadosUsuario();
        
        return informacoes;
        
    }

    // Método que retorna a quantidade de nós presentes na fila.
    public int size(){
        
        return this.getQuantidadeDeNos(); 
        
    }
    
    
    // Método para pegar o nome da pessoa na primeiro posição da fila.
    public String getNomePrimeiro(){
        return Fila[0].getObjeto().getNome();
    }
    
    // Método para pegar o email da pessoa na primeiro posição da fila.
    public String getEmailPrimeiro(){
        return Fila[0].getObjeto().getEmail();
    }
    
    // Método para pegar o número da pessoa na primeiro posição da fila.
    public long NumeroPrimeiro(){
        return Fila[0].getObjeto().getNumero();
    }
}

