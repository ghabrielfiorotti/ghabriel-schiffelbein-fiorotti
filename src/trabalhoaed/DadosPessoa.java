package trabalhoaed;

/**
 *
 * @author ghabr
 */

class DadosPessoa {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    private String nomeUsuario;
    private String email;
    private long numero;
    private int numSala;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public DadosPessoa(){ }
    
    public DadosPessoa(String _nomeU, String email, long numero, int numberSala) {
        this.nomeUsuario   = _nomeU;
        this.email = email;
        this.numero = numero;
        this.numSala = numberSala;
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para resgatar o email do usuário
    public String getEmail() {
        return this.email;
    }

    // Método para resgatar o nome do usuário
    public String getNome() {
        return this.nomeUsuario;
    }
    
    // Método para resgatar o numero do usuário
    public long getNumero() {
        return this.numero;
    }
    
    public int getNumSala(){
        return this.numSala;
    }

    
    public String toStringDadosUsuario(){

        String informacoes = "";

        informacoes  = "                           Nome: " + this.getNome()+ "\n";
        informacoes += "                           Email: " + this.getEmail()+ "\n";
        informacoes += "                           Telefone: " + this.getNumero()+ "\n";
        informacoes += "                           Número da Sala: " + this.getNumSala()+ "\n\n";


        return informacoes;

    }
}
