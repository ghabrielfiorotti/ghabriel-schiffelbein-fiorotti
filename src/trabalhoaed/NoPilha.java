package trabalhoaed;

/**
 *
 * @author ghabr
 */
class NoPilha {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/

    private Ingressos objeto;
    private NoPilha referencia;

    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // numberSala = Número da sala
    public NoPilha(int numberSala) {
        Ingressos u = new Ingressos(numberSala);
        this.setObjeto(u);
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/

    // Método set para registrar a referência do próximo nó da pilha.
    // _rPN = referência do próximo nó
    public void setProximo(NoPilha _rPN) {
        this.referencia = _rPN;
    } 

    // Método get para resgatar a referência do próximo nó da pilha.
    public NoPilha getProximo() {
        return this.referencia;
    }

    // Método set para registrar a instância do usuário.
    // _u = usuário
    public void setObjeto(Ingressos _u){
        this.objeto = _u;
    }
    
    // Método get para resgatar o objeto Ingressos.
    public Ingressos getObjeto() {
        return this.objeto;
    }
    
}