package trabalhoaed;

/**
 *
 * @author ghabr
 */
class NoFila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/

    private Pessoa objeto;
    private NoFila referencia;

    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // _email = email
    // _n = nome
    // num = número
    public NoFila(String _n, String _mail, long num) {
        
        Pessoa u = new Pessoa(_n, _mail, num);
        this.setObjeto(u);
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/

    // Método set para registrar a referência do próximo nó da pilha.
    // _rPN = referência do próximo nó
    public void setProximo(NoFila _rPN) {
        this.referencia = _rPN;
    } 

    // Método get para resgatar a referência do próximo nó da pilha.
    public NoFila getProximo() {
        return this.referencia;
    }

    // Método set para registrar a instância do usuário.
    // _u = usuário
    public void setObjeto(Pessoa _u){
        this.objeto = _u;
    }
    
    // Método get para resgatar o objeto Pessoa.
    public Pessoa getObjeto() {
        return this.objeto;
    }
    
    
}
