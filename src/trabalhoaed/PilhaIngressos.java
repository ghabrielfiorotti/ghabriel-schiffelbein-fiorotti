package trabalhoaed;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
/**
 *
 * @author ghabr
 */
public class PilhaIngressos {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/

    public int qtdSala1 = 0;
        int qtdSala2 = 0;
        int qtdSala3 = 0;
        int qtdSala4 = 0;
        int qtdSala5 = 0;
        NoPilha topo;
        int quantidadeDeNos;
 
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
   
    public PilhaIngressos(){ }
    
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    
    // Método para acessar o topo da pilha.
    public NoPilha getTopo() {
        return this.topo;
    }

    // Método para alterar (registrar) o topo da pilha.
    public void setTopo(NoPilha _topo) {
        this.topo = _topo;
    }
    
    // Método para acessar a quantidade de nós da pilha.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }
    
    // Método para atualizar a quantidade de nós da pilha.
    // _qDN = quantidade de nós
    public void setQuantidadeDeNos(int _qDN) {
        this.quantidadeDeNos = _qDN;
    }
    
    /*-----------------------------------
           OPERAÇÕES DO TAD PILHA E
    -----------------------------------*/
    
    // Método para criar a pilha encadeada.
    public void criarPilha(){
        this.setTopo(null);
        this.setQuantidadeDeNos(0);
    }

    // Método para verificar se a pilha de ingressos está vazia.
    public boolean isEmpty(){
        return this.getTopo() == null;
    }
    
    // Método para gerar número Aleatório
    public int gerar(){
            Random rnd = ThreadLocalRandom.current();
            int numeroAleatorio =  (rnd.nextInt(5 - 1 + 1) + 1);
            
            if(numeroAleatorio == 1){
                if(qtdSala1 == 13)
                    gerar();
                else
                    qtdSala1++;
            }
            if(numeroAleatorio == 1){
                if(qtdSala2 == 13)
                    gerar();
                else
                    qtdSala2++;
            }
            if(numeroAleatorio == 1){
                if(qtdSala3 == 13)
                    gerar();
                else
                    qtdSala3++;
            }
            if(numeroAleatorio == 1){
                if(qtdSala4 == 13)
                    gerar();
                else
                    qtdSala4++;
            }
            if(numeroAleatorio == 1){
                if(qtdSala5 == 13)
                    gerar();
                else
                    qtdSala5++;
            }

            return numeroAleatorio;
        
    }
    // Método para criar a Pilha de ingressos em ordem aleatória
    public void push (){
        for(int x =0; x <65; x++){
            NoPilha novoNo = new NoPilha(gerar());

            // Se a pilha não estiver vazia,
            // novoNo vai apontar para o nó que está no topo da pilha,
            // Ou seja, guarda o endereço do nó que está no topo da pilha.
            if (!this.isEmpty())
                novoNo.setProximo(this.getTopo());
            else
                novoNo.setProximo(null);

            // Atualiza o topo da pilha:
            // - novoNo agora é o nó que está no topo da pilha.
            this.setTopo(novoNo);

            // Atualiza a quantidade de nós da pilha.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()+1);
        }
    }
    
    
    
    // Método para desempilhar dados da pilha.
    public boolean pop(){

        // Se foi possível apresentar os dados do nó
        // Que está no topo, efetiva a remoção.
        if (this.top()){

            // Recebe o endereço de quem está no topo da pilha.
            NoPilha auxiliar = this.getTopo();

            // O nó que ficará no topo da pilha é o que vem
            // depois do 1º nó (o que estava no topo).
            this.setTopo(auxiliar.getProximo());

            // Desconecta o nó (controlado por 'auxiliar') da pilha.
            auxiliar.setProximo(null);

            // Atualiza a quantidade de nós da pilha.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()-1);
            return true;

        }

        return false;

    }
    
    // Método para apresentar os dados de quem está no topo da pilha.
    public boolean top(){
        
        // Se a pilha não estiver vazia,
        // apresenta os dados do nó que está no topo da pilha.
        if (!this.isEmpty()){
            System.out.println(this.getTopo().getObjeto().getNumeroSala());
            
            return true;

        }
        
        return false;
        
    }
    
    // Mostrando a quantidade de ingressos na pilha.
    public int size(){
        return this.getQuantidadeDeNos();
    }
    
    // Esvaziando a pilha encadeada.
    public boolean clear(){
        this.setTopo(null);
        this.setQuantidadeDeNos(0);
        
        return this.getTopo() == null;
    }
    
    // Mostrando os dados da pilha encadeada.
    public boolean print(){
        
        // Se a pilha não estiver vazia,
        if (!this.isEmpty()){
            
            // Cria uma cópia do endereço de quem está no topo
            // e coloca no atributo 'auxiliar'.
            NoPilha auxiliar = this.getTopo();
            
            // Enquanto o endereço de 'auxiliar'
            // é diferente de null, percorre a pilha.
            while (auxiliar != null){
                
                System.out.println(auxiliar.getObjeto().getNumeroSala());


                // Avança para o próximo nó da pilha.
                auxiliar = auxiliar.getProximo();
                
            }
            
            return true;
            
        }
        
        return false;
        
    }
    
    
    //Método para acessar Número da sala que está no topo da Pilha de Ingressos;
    public int getNumSalaTop(){
        return getTopo().getObjeto().getNumeroSala();
    }
    
}