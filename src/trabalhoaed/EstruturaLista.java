package trabalhoaed;

class EstruturaLista {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private DadosPessoa[] ListaCompleta; // Atributo lista do tipo DadosPessoa
    
    // Descritores da lista contígua
    private int FimDaLista;
    private int TamanhoDaLista;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // Construtor da classe
    // tl = Tamanho da lista
    public EstruturaLista(int _tl) {
        this.criarLista(_tl);
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método que registra a instância da lista DadosPessoa.
    public DadosPessoa[] getLista(){
        return this.ListaCompleta;
    }
    
    // Método que retorna a lista DadosPessoa.
    public void setLista(DadosPessoa[] _lu){
        this.ListaCompleta = _lu;
    }
    
    // Método que retorna a posição do último elemento da lista.
    public int getFimDaLista() {
        return FimDaLista;
    }

    // Método para alterar o atributo FimDaLista.
    public void setFimDaLista(int _fl) {
        this.FimDaLista = _fl;
    }
    
    // Método que retorna o tamanho da lista.
    public int getTamanhoDaLista() {
        return TamanhoDaLista;
    }
    
    // Método para alterar o atributo TamanhoDaLista.
    public void setTamanhoDaLista(int _tl) {
        this.TamanhoDaLista = _tl;
    }

    /*-----------------------------------
            OPERAÇÕES DO TAD LISTA CD
      -----------------------------------*/
    
    // Método para criar a lista contígua.
    private void criarLista(int _tl){
        
        // Registrando a instância da lista em tempo de execução
        this.setLista(new DadosPessoa[_tl]);
        
        this.setFimDaLista(-1);
        this.setTamanhoDaLista(_tl);
        
    }
    
    // Método para limpar a lista contígua.
    public void limparLista(){
        this.setFimDaLista(-1);
    }
    
    // Método que verifica se a lista está vazia.
    public boolean isEmpty(){
        
        // Se o fim da lista tiver o valor -1
        // quer dizer que a lista está vazia.
        return this.getFimDaLista() == -1;
        
    }
    
    // Método que verifica se a lista está cheia.
    public boolean isFull(){
        
        // Se o fim da lista for exatamente o tamanho da lista
        // quer dizer que a lista está cheia.
        return this.getFimDaLista() == this.getTamanhoDaLista()-1;
        
    }
    
    // Método para inserir dados na lista contígua.
    public boolean inserirDados(int p, String nome, String mail, long num, int numSala){
        
        // Se a Lista estiver cheia ou
        // posição negativa ou
        // posição maior que o fim da lista (deixando buraco)
        if((this.isFull()) || (p < 0) || (p > this.getFimDaLista()+1)){
            
            return false; // Quer dizer que não será possível inserir dados
        
        }else{
            
            // Cria-se uma nova instância da classe 'Usuario' e já insere os dados
            DadosPessoa dados = new DadosPessoa(nome, mail, num, numSala);
            
            // Se o usuário deseja inserir na próxima posíção válida
            if (p == (this.getFimDaLista()+1)){
                
                this.setFimDaLista(this.getFimDaLista()+1); // Define a posição
                this.getLista()[p] = dados; // Insere o objeto inteiro na posição 'p'
            
            }else{
                
                // Caso contrário, percorre do fim da lista até a posição 'p'
                // com o objetivo de abrir um espaço na lista para inserir o objeto
                // sem perder os outros objetos.
                for (int i = this.getFimDaLista(); i >= p; i--)
                    this.getLista()[i+1] = this.getLista()[i];
                                
                this.getLista()[p] = dados; // Insere o objeto
                this.setFimDaLista(this.getFimDaLista()+1); // Atualiza o fim da lista
                
            }

            return true;
            
        }
        
    }
    
    // Método para remover dados da lista contígua.
    public boolean removerDados(int p){
        
        // Se a Lista estiver vazia ou
        // posição negativa ou
        // posição maior que o fim da lista
        if ((this.isEmpty()) || (p < 0) || (p > this.getTamanhoDaLista())){
        
            return false; // Retorna dizendo que não foi possível remover
        
        }else{
            
            // Se a posição for exatamente o fim da lista, basta
            // decrementar em 1 unidade o atributo Fim da Lista
            if (p == this.getFimDaLista()){
                
                this.setFimDaLista(this.getFimDaLista()-1);
            
            }else{
                
                // Caso contrário, percorre da posição 'p' até o penúltimo elemento da lista
                // Pega o próximo elemento para a posição anterior
                for(int i = p; i < this.getFimDaLista() ; i++)
                    this.getLista()[i] = this.getLista()[i+1];
                
                this.setFimDaLista(this.getFimDaLista()-1); // Decrementa em 1 unidade o atributo Fim da Lista
            }   
            
            return true; // Retorna dizendo que foi possível remover
            
        }
        
    }
    
    // Método para mostrar todos os dados da lista contígua
    public String mostrarDados(){
        String informacoes = "";
        if(this.isEmpty()==true)
            return ("                      Ninguém agraciado no momento \n");
        else{
            for(int x = 0; x <= getFimDaLista();x++){
                informacoes += (ListaCompleta[x].toStringDadosUsuario());
            }        
        }
        return informacoes;   
    }
}