package trabalhoaed;

/**
 *
 * @author ghabr
 */
public class Pessoa {
    private String nomeUsuario;
    private String email;
    private long numero;
        
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/

    // _nomeU = nome do usuário
    // email  = email do usuário
    // numero  = número do usuário
    public Pessoa(String _nomeU, String email, long numero) {
        this.nomeUsuario   = _nomeU;
        this.email = email;
        this.numero = numero;
    }

    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para resgatar o email do usuário
    public String getEmail() {
        return this.email;
    }

    // Método para resgatar o nome do usuário
    public String getNome() {
        return this.nomeUsuario;
    }
    
    // Método para resgatar o número do usuário
    public long getNumero() {
        return this.numero;
    }
    
    
    // Método para resgatar em String todos os dados dessa classe
    public String toStringDadosUsuario(){

        String informacoes = "";

        informacoes  = "Nome: " + this.getNome()+ "\n";
        informacoes += "Email:   " + this.getEmail()+ "\n";
        informacoes += "Telefone:   " + this.getNumero()+ "\n\n";

        return informacoes;

    }
}